# Suoratoistopalvelut

Back-End ohjelma jolla käyttäjä voi luoda listaa eri suoratoistopalveluissa olevista ohjelmista.
Listaan voi lisätä Ohjelman nimen, genren, palvelun nimen mistä ohjelma löytyy sekä onko se sarja vai elokuva.

## Teknologiat

- C#
- .NET Core
- ASP.NET Core
- Entity Framework Core
- Node.js
- PostgreSQL 15

## PostgreSQL

Tämä ohjelmisto käyttää PostgreSQL-tietokantaa. Asennustiedoston saat tästä linkistä https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

## Backend Käyttöönotto

Tämä ohje edellyttää Visual Studion käyttämistä. Tämä projekti käyttää .NET SDK 6.

Avaa Suoratoistopalvelut.sln Visual Studiossa. Sinun täytyy lisätä database connection string User Secrets tiedostoon. Klikkaa hiiren oikealla napilla projektiasi Visual Studiossa ja valitse avautuvasta valikosta "Manage User Secrets", tai käytä dotnet CLI:tä `dotnet user-secrets set <key> <value>`. Käytä key:nä `"DbConnectionString"`.

Esimerkki:

```
dotnet user-secrets set "DbConnectionString" "Server=PostgreSQL 15;Host=localhost;Port=5432;Username=postgres;Password=12345;Database=database"
```

Sama tarvitsee tehdä token key:lle käyttämällä `"Jwt:Key"`, key:n pitää olla vähintään 16 merkkiä pitkä.

Komento esimerkki

```
dotnet user-secrets set "Jwt:Key" "ThisIs16Charssss"
 ```

Esimerkki User Secrets tiedostosta

```json
{
  "DbConnectionString": "Server=PostgreSQL 15;Host=localhost;Port=5432;Username=postgres;Password=12345;Database=database_db",
    "Jwt": {
    "Key":  "SecureKeyThatIsAtLeast16Characters"
  }
}
```

Seuraavaksi aja käsky `Update-Database` Package Manage Consolissa.

### Swagger

Käyttäessäsi Swaggeria sinun täytyy ensin käyttää User - /api/Users/Login endpointia kirjautumiseen jollakin olemassaolevalla käyttäjällä ja kopioida saamasi token "Authorize" napista avautuvaan kenttään seuraavan esimerkin mukaan

```
Bearer <token>
```

Voit käyttää User - /api/Users/Register endpointia uuden käyttäjän luomiseen ja käyttää sitä kirjautumiseen.

![Authorize button in Swagger](swagger_authorization.png "Authorize button")


### Backend:in asetus ilman Visual Studioa

Run `install_dotnet_etc.bat` asentaaksesi tarvittavat .NET ja ASP.NET paketit winget:ssä. Tämä tarvitsee tehdä vain kerran.

Run `setup_user_secrets.bat` ja konfiguroi Database Connection Stringisi (katso ylempänä olevaa esimerkkiä). Jos sinun tarvitsee käyttää välilyönteja, niin laita arvo(value) lainausmerkkien sisään.

Run `update_database.bat` luodaksesi tietokannan taulut ensimmäisellä kerralla. Lisäksi tämä tulee ajaa jokaisen mahdollisen migraation jälkeen pitääksesi tietokantasi ajantasalla.

Run `start_backend_server.bat` käynnistääksesi serverin omalla koneellasi. Käynnistyksen jälkeen serveri kertoo mistä se odottaa pyyntöjä (by default https://localhost:7154  and http://localhost:5191). Käyttääksesi Swaggeria, lisää `/swagger` URL:in jälkeen (esim. https://localhost:7154/swagger).

---

## API Endpoint Routes


### Users

```
- GET     /api/Users/GetUsers - Get all users

- GET     /api/Users/GetUserById/{id} - Get user by id

- DELETE  /api/Users/DeleteUser - Delete currently logged in user

- PUT     /api/Users/UpdateUserByPut - Update currently logged in user
                        (can change displayName, email and imageUrl)

- PATCH   /api/Users/PartiallyUpdateUserByPatch - Patch currently user
                        (can change userName, password and email)

- POST    /api/Users/Register - Register a new user (allowed without token)
          ->  { username: "username", password: "password" }

          optional parameters: { displayname: "displayname",
                                 email: "email",
                                 imageurl: "imageurl"}

- POST    /api/Users/Login - Login and receive token (allowed without token)
          -> { username: "username", password: "password" }
          -> returns token string and user id in response to successful login

- POST    /api/Users/ChangePassword - Change password for currently logged in user
                                      (uses token in backend to check user id)
          -> { oldpassword: "oldpassword", newpassword: "newpassword" }
```

### Services

```
- GET     /api/Services/GetServices - Get all services (allowed without token)

- GET     /api/Services/GetServiceById/{id} - Get service by id

- POST   /api/Services/AddNewServiceByPOST     - Create a new service
         -> { name: "name" }

- DELETE  /api/Services/DeleteService/{id} - Delete service by id

- PUT    /api/Services/UpdateServiceByPut/{id} - Update service name by id
```

### Repertorys

```
- GET     /api/Repertorys/GetByRepertoryName - Get repertory by name

- GET     /api/Repertorys/GetRepertorys - Get all repertorys

- GET     /api/Repertorys/GetRepertoryById/{id} - Get repertory by id

- GET     /api/Repertorys/GetRepertorysByServiceName - Get repertorys by serviceName

- GET     /api/Repertorys/GetRepertorysByServiceId - Get repertorys by serviceId

- GET     /api/Repertorys/GetRepertorysByGenreName - Get repertorys by genreName

- GET     /api/Repertorys/GetRepertorysByGenreId - Get repertorys by genreId

- POST   /api/Repertorys/AddNewRepertoryByPOST     - Create a new repertory
         -> { name: "name" }
            { addDay: "addDay" }
            { serie_Movie: "serie_Movie" }
            { serviceName: "serviceName" }
            { genreName: "genreName" }

- DELETE  /api/Repertorys/DeleteRepertory/{id} - Delete repertory by id

- PUT    /api/Repertorys/UpdateRepertoryByPut/{id} - Update repertory by id
         -> { name: "name" }
            { addDay: "addDay" }
            { serie_Movie: "serie_Movie" }
            { serviceName: "serviceName" }
            { genreName: "genreName" }

- PATCH   /api/Genres/PartiallyUpdateRepertoryByPatch/{id} - Patch repertory by id
```

### Genres

```
- GET     /api/Genres/GetGenres - Get all genres (allowed without token)

- GET     /api/Genres/GetGenreById/{id} - Get genre by id

- POST   /api/Genres/AddNewGenreByPOST     - Create a new genre
         -> { name: "name" }

- DELETE  /api/Genres/DeleteGenre/{id} - Delete genre by id

- PUT    /api/Genres/UpdateGenreByPut/{id} - Update genre name by id
```

## TODO

- Lisätä endpointteihin user Id, siten että kirjautunut käyttäjä voi luoda, muokata tai poistaa vain itse tekemiään ohjelmatietoja.
- Endpoint joka hakee Ohjelmatiedot(repertorys) jotka kuuluvat kirjautuneelle käyttäjälle (user Id:n tarkistamiseen käytetään tokenia)
- Testien toteuttaminen mahdollisimman moneen endpointiin
