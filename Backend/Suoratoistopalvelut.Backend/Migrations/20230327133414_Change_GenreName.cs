﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Suoratoistopalvelut.Migrations
{
    public partial class Change_GenreName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Genres",
                newName: "GenreName");

            migrationBuilder.AddColumn<string>(
                name: "GenreName",
                table: "Repertorys",
                type: "text",
                nullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Genres_GenreName",
                table: "Genres",
                column: "GenreName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_Genres_GenreName",
                table: "Genres");

            migrationBuilder.DropColumn(
                name: "GenreName",
                table: "Repertorys");

            migrationBuilder.RenameColumn(
                name: "GenreName",
                table: "Genres",
                newName: "Name");
        }
    }
}
