﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Suoratoistopalvelut.Migrations
{
    public partial class Deleted_GenreName_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GenreName",
                table: "Repertorys");

            migrationBuilder.CreateIndex(
                name: "IX_Repertorys_GenreId",
                table: "Repertorys",
                column: "GenreId");

            migrationBuilder.AddForeignKey(
                name: "FK_Repertorys_Genres_GenreId",
                table: "Repertorys",
                column: "GenreId",
                principalTable: "Genres",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Repertorys_Genres_GenreId",
                table: "Repertorys");

            migrationBuilder.DropIndex(
                name: "IX_Repertorys_GenreId",
                table: "Repertorys");

            migrationBuilder.AddColumn<string>(
                name: "GenreName",
                table: "Repertorys",
                type: "text",
                nullable: true);
        }
    }
}
