﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Suoratoistopalvelut.Migrations
{
    public partial class Changed_ServiceName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Services",
                newName: "ServiceName");

            migrationBuilder.AlterColumn<DateTime>(
                name: "AddDay",
                table: "Repertorys",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Repertorys_ServiceId",
                table: "Repertorys",
                column: "ServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Repertorys_Services_ServiceId",
                table: "Repertorys",
                column: "ServiceId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Repertorys_Services_ServiceId",
                table: "Repertorys");

            migrationBuilder.DropIndex(
                name: "IX_Repertorys_ServiceId",
                table: "Repertorys");

            migrationBuilder.RenameColumn(
                name: "ServiceName",
                table: "Services",
                newName: "Name");

            migrationBuilder.AlterColumn<DateTime>(
                name: "AddDay",
                table: "Repertorys",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");
        }
    }
}
