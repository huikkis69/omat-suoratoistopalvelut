﻿using System.ComponentModel.DataAnnotations;

namespace Suoratoistopalvelut.Models
{
    public partial class Genre : IEntity
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "{0}:lla on oltava jotain tekstiä.")]
        public string? GenreName { get; set; }
    }
}
