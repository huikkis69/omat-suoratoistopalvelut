﻿using System.ComponentModel.DataAnnotations;

namespace Suoratoistopalvelut.Models
{
    public class PasswordChangeData
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }
}
