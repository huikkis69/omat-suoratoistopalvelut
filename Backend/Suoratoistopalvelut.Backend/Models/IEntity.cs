﻿namespace Suoratoistopalvelut.Models
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
