﻿using System.ComponentModel.DataAnnotations;

namespace Suoratoistopalvelut.Models
{
    public class UserUpdate
    {
        public string? DisplayName { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email address.")]
        public string? Email { get; set; }
        public string? ImageUrl { get; set; }
    }
}
