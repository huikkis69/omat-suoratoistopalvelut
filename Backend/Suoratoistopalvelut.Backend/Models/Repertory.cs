﻿using System.ComponentModel.DataAnnotations;
using Microsoft.VisualBasic;
using Suoratoistopalvelut.Migrations;

namespace Suoratoistopalvelut.Models
{
    public partial class Repertory : IEntity
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "{0}:llä on oltava jotain tekstiä.")]
        public string? Name { get; set; }

        [Required]
        public int ServiceId { get; set; }

        [Required]
        public int GenreId { get; set; }

        public DateTime AddDay { get; set; }

        [Required(ErrorMessage = "{0} ei voi olla tyhjä.")]
        public string? Serie_Movie { get; set; }
        public Service? Service { get; set; }
        public Genre? Genre { get; set; }
    }
}
