﻿using System.ComponentModel.DataAnnotations;

namespace Suoratoistopalvelut.Models
{
    public struct LoginCredentials
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
