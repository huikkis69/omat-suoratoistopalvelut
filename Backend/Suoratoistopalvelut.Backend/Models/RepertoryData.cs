﻿using System.ComponentModel.DataAnnotations;
using Microsoft.VisualBasic;
using Suoratoistopalvelut.Migrations;

namespace Suoratoistopalvelut.Models
{
    public class RepertoryData
    {
        [Required(ErrorMessage = "{0}:llä on oltava jotain tekstiä.")]
        public string? Name { get; set; }

        public DateTime AddDay { get; set; }

        [Required(ErrorMessage = "{0} ei voi olla tyhjä.")]
        public string? Serie_Movie { get; set; }

        [Required]
        public string? ServiceName { get; set; }

        [Required]
        public string? GenreName { get; set; }
    }
}
