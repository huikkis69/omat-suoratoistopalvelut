﻿using System.ComponentModel.DataAnnotations;

namespace Suoratoistopalvelut.Models
{
    public partial class Service : IEntity
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "{0}:llä on oltava jotain tekstiä.")]
        public string? ServiceName { get; set; }
    }
}
