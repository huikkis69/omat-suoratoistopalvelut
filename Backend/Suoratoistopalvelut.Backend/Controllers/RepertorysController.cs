﻿using System.Globalization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Suoratoistopalvelut.Models;
using Suoratoistopalvelut.Repositories;
using Suoratoistopalvelut.Services;

namespace Suoratoistopalvelut.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RepertorysController : ControllerBase
    {
        private readonly IRepertoryService _repertoryService;
        private readonly IServiceService _serviceService;
        private readonly IGenreService _genreService;

        public RepertorysController(IRepertoryService repertoryService, IServiceService serviceService, IGenreService genreService)
        {
            _repertoryService = repertoryService;
            _serviceService = serviceService;
            _genreService = genreService;
        }

        [HttpGet("GetByRepertoryName")]
        public IActionResult GetByRepertoryName(string repertoryName)
        {
            var selectedRepertoryName = _repertoryService.GetByRepertoryName(repertoryName);

            if (selectedRepertoryName == null)
            {
                return NotFound("Tällä nimellä ei ole Ohjelmaa");
            }
            return Ok(selectedRepertoryName);
        }

        [HttpGet("GetRepertorys")]
        public IActionResult GetAllRepertorys()
        {
            return Ok(_repertoryService.GetAllRepertorys());
        }

        [HttpGet("GetRepertoryById/{id}")]
        public IActionResult GetRepertoryById(int id)
        {
            Repertory repertory = _repertoryService.GetRepertoryById(id);
            if (repertory == null)
            {
                return NotFound();
            }
            return Ok(repertory);
        }

        [HttpGet("GetRepertorysByServiceName")]
        public IActionResult GetRepertorysByServiceName(string serviceName)
        {
            var selectedServiceName = _serviceService.GetByServiceName(serviceName);
            
            if (selectedServiceName == null)
            {
                return NotFound("Tällä nimellä ei ole Serviceä");
            }

            var repertorysByServiceName = _repertoryService.GetRepertorysByServiceName(selectedServiceName);

            if (repertorysByServiceName.Count == 0)
            {
                return NotFound("Tällä Service nimellä ei ole Ohjelmia");
            }

            return Ok(repertorysByServiceName);
        }

        [HttpGet("GetRepertorysByServiceId")]
        public IActionResult GetRepertorysByServiceId(int serviceId)
        {
            var selectedServiceId = _serviceService.GetById(serviceId);

            if (selectedServiceId == null)
            {
                return NotFound("Tällä Id:llä ei ole Serviceä");
            }

            var serviceRepertorys = _repertoryService.GetRepertorysByServiceId(serviceId);
            if (serviceRepertorys.Count == 0)
            {
                return NotFound("Tällä ServiceId:llä ei ole Ohjelmia");
            }

            return Ok(serviceRepertorys);
        }

        [HttpGet("GetRepertorysByGenreName")]
        public IActionResult GetRepertorysByGenreName(string genreName)
        {
            var selectedGenreName = _genreService.GetByGenreName(genreName);
            
            if (selectedGenreName == null)
            {
                return NotFound("Tällä nimellä ei ole Genreä");
            }

            var repertorysByGenreName = _repertoryService.GetRepertorysByGenreName(selectedGenreName);

            if (repertorysByGenreName.Count == 0)
            {
                return NotFound("Tällä Genre nimellä ei ole Ohjelmia");
            }

            return Ok(repertorysByGenreName);
        }

        [HttpGet("GetRepertorysByGenreId")]
        public IActionResult GetRepertorysByGenreId(int genreId)
        {
            var selectedGenreId = _genreService.GetById(genreId);

            if (selectedGenreId == null)
            {
                return NotFound("Tällä Id:llä ei ole Genreä");
            }

            var genreRepertorys = _repertoryService.GetRepertorysByGenreId(genreId);
            if (genreRepertorys.Count == 0)
            {
                return NotFound("Tällä GenreId:llä ei ole Ohjelmia");
            }

            return Ok(genreRepertorys);
        }

        [HttpPost("AddNewRepertoryByPOST")]
        public IActionResult Add([FromBody] RepertoryData repertoryData)
        {
            if (_repertoryService.GetByRepertoryName(repertoryData.Name) != null)
            {
                return BadRequest("Tällä nimellä on jo Ohjelma");
            }

            if (!TryValidateModel(repertoryData))
            {
                return BadRequest();
            }
            _repertoryService.Add(repertoryData);
            return Created(Request.Path, repertoryData);
        }

        [HttpDelete("DeleteRepertory/{id}")]
        public IActionResult Delete(int id)
        {
            if (!_repertoryService.GetAllRepertorys().Any(c => c.Id == id))
            {
                return NotFound();
            }
            _repertoryService.Delete(id);
            return Ok(_repertoryService.GetAllRepertorys());
        }

        [HttpPut("UpdateRepertoryByPut/{id}")]
        public IActionResult Update(int id, [FromBody] RepertoryData repertoryData)
        {
            if (!_repertoryService.GetAllRepertorys().Any(c => c.Id == id))
            {
                return NotFound("Tällä ID:llä ei löydy Ohjelmaa");
            }

            if (!TryValidateModel(repertoryData))
            {
                return BadRequest();
            }

            _repertoryService.Update(id, new RepertoryData
            {
                Name = repertoryData.Name,
                AddDay = repertoryData.AddDay,
                Serie_Movie = repertoryData.Serie_Movie,
                ServiceName = repertoryData.ServiceName,
                GenreName = repertoryData.GenreName
            });
            return Ok(_repertoryService.GetRepertoryById(id));
        }

        [HttpPatch("PartiallyUpdateRepertoryByPatch/{id}")]
        public IActionResult PartiallyUpdateRepertory(int id, [FromBody] JsonPatchDocument<RepertoryData> patchDocument)
        {
            var selectedRepertory = _repertoryService.GetRepertoryById(id);

            if (selectedRepertory == null)
            {
                return NotFound();
            }

            var selectedRepertoryData = new RepertoryData()
            {
                Name = selectedRepertory.Name,
                AddDay = selectedRepertory.AddDay,
                Serie_Movie = selectedRepertory.Serie_Movie,
                ServiceName = selectedRepertory.Service.ServiceName,
                GenreName = selectedRepertory.Genre.GenreName
            };

            patchDocument.ApplyTo(selectedRepertoryData);

            var serviceName = _serviceService.GetByServiceName(selectedRepertoryData.ServiceName);
            if (serviceName == null)
            {
                _serviceService.Add(
                new Service
                {
                    ServiceName = selectedRepertoryData.ServiceName
                });
            }

            var genreName = _genreService.GetByGenreName(selectedRepertoryData.GenreName);
            if (genreName == null)
            {
                _genreService.Add(
                new Genre
                {
                    GenreName = selectedRepertoryData.GenreName
                });
            }

            if (!TryValidateModel(selectedRepertoryData))
            {
                return BadRequest();
            }

            _repertoryService.Update(id, selectedRepertoryData);
            return Ok(_repertoryService.GetRepertoryById(id));
        }
    }
}
