﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Suoratoistopalvelut.Models;
using Suoratoistopalvelut.Services;

namespace Suoratoistopalvelut.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GenresController : ControllerBase
    {
        private readonly IGenreService _genreService;
        private readonly IRepertoryService _repertoryService;

        public GenresController(IGenreService genreService, IRepertoryService repertoryService)
        {
            _genreService = genreService;
            _repertoryService = repertoryService;
        }

        [AllowAnonymous]
        [HttpGet("GetGenres")]
        public IActionResult GetAll()
        {
            return Ok(_genreService.GetAll());
        }

        [HttpGet("GetGenreById/{id}")]
        public IActionResult GetById(int id)
        {
            Genre genre = _genreService.GetById(id);
            if (genre == null)
            {
                return NotFound();
            }
            return Ok(genre);
        }

        [HttpPost("AddNewGenreByPOST")]
        public IActionResult Add([FromBody] Genre genre)
        {
            if (_genreService.GetByGenreName(genre.GenreName) != null)
            {
                return BadRequest("Tällä nimellä on jo Genre");
            }

            if (!TryValidateModel(genre))
            {
                return BadRequest();
            }
            _genreService.Add(
                new Genre
                {
                    GenreName = genre.GenreName
                }
            );
            return Created(Request.Path, genre);
        }

        [HttpDelete("DeleteGenre/{id}")]
        public IActionResult Delete(int id)
        {
            if (!_genreService.GetAll().Any(c => c.Id == id))
            {
                return NotFound();
            }

            var genreRepertorys = _repertoryService.GetRepertorysByGenreId(id);
            if (genreRepertorys.Count != 0)
            {
                return BadRequest("Löytyi Ohjelmatietoja joissa on tämä Genre. Muuta niitä ennen Genren poistoa.");
            }

            _genreService.Delete(id);
            return Ok(_genreService.GetAll());
        }

        [HttpPut("UpdateGenreByPut/{id}")]
        public IActionResult Update(int id, [FromBody] Genre genre)
        {
            if (!_genreService.GetAll().Any(c => c.Id == id))
            {
                return NotFound("Tällä ID:llä ei löydy Genreä");
            }

            if (!TryValidateModel(genre))
            {
                return BadRequest();
            }

            _genreService.Update(id, new Genre
            {
                GenreName = genre.GenreName
            }
            );
            return NoContent();
        }
    }
}
