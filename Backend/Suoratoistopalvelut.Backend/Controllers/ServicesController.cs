﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Suoratoistopalvelut.Models;
using Suoratoistopalvelut.Services;

namespace Suoratoistopalvelut.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ServicesController : ControllerBase
    {
        private readonly IServiceService _serviceService;
        private readonly IRepertoryService _repertoryService;

        public ServicesController(IServiceService serviceService, IRepertoryService repertoryService)
        {
            _serviceService = serviceService;
            _repertoryService = repertoryService;
        }

        [AllowAnonymous]
        [HttpGet("GetServices")]
        public IActionResult GetAll()
        {
            return Ok(_serviceService.GetAll());
        }

        [HttpGet("GetServiceById/{id}")]
        public IActionResult GetById(int id)
        {
            Service service = _serviceService.GetById(id);
            if (service == null)
            {
                return NotFound();
            }
            return Ok(service);
        }

        [HttpPost("AddNewServiceByPOST")]
        public IActionResult Add([FromBody] Service service)
        {
            if (_serviceService.GetByServiceName(service.ServiceName) != null)
            {
                return BadRequest("Tällä nimellä on jo Service");
            }

            if (!TryValidateModel(service))
            {
                return BadRequest();
            }
            _serviceService.Add(
                new Service
                {
                    ServiceName = service.ServiceName
                }
            );
            return Created(Request.Path, service);
        }

        [HttpDelete("DeleteService/{id}")]
        public IActionResult Delete(int id)
        {
            if (!_serviceService.GetAll().Any(c => c.Id == id))
            {
                return NotFound();
            }

            var serviceRepertorys = _repertoryService.GetRepertorysByServiceId(id);
            if (serviceRepertorys.Count != 0)
            {
                return BadRequest("Löytyi Ohjelmatietoja joissa on tämä Service. Muuta niitä ennen Servicen poistoa.");
            }

            _serviceService.Delete(id);
            return Ok(_serviceService.GetAll());
        }

        [HttpPut("UpdateServiceByPut/{id}")]
        public IActionResult Update(int id, [FromBody] Service service)
        {
            if (!_serviceService.GetAll().Any(c => c.Id == id))
            {
                return NotFound("Tällä ID:llä ei löydy Serviceä");
            }

            if (!TryValidateModel(service))
            {
                return BadRequest();
            }

            _serviceService.Update(id, new Service
            {
                ServiceName = service.ServiceName
            }
            );
            return NoContent();
        }
    }
}
