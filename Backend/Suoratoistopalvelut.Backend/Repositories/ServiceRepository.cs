﻿using Suoratoistopalvelut.Models;

namespace Suoratoistopalvelut.Repositories
{
    public class ServiceRepository : RepoBase<Service>, IServiceRepository
    {
        public ServiceRepository(Streaming_servicesContext context) : base(context) { }
        public Service GetByServiceName(string serviceName)
        {
            return _context.Set<Service>().FirstOrDefault(entity => entity.ServiceName == serviceName);
        }
    }
}
