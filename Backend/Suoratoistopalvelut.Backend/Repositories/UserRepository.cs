﻿using Suoratoistopalvelut.Models;

namespace Suoratoistopalvelut.Repositories
{
    public class UserRepository : RepoBase<User>, IUserRepository
    {
        public UserRepository(Streaming_servicesContext context) : base(context) { }

        public User GetByEmail(string email)
        {
            return _context.Set<User>().FirstOrDefault(entity => entity.Email == email);
        }

        public User GetByUsername(string userName)
        {
            return _context.Set<User>().FirstOrDefault(entity => entity.UserName == userName);
        }
    }
}
