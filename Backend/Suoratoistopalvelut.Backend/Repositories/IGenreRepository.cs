﻿using Suoratoistopalvelut.Models;

namespace Suoratoistopalvelut.Repositories
{
    public interface IGenreRepository : IRepository<Genre>
    {
        public Genre? GetByGenreName(string genreName);
    }
}
