using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Suoratoistopalvelut.Models;
using Suoratoistopalvelut.Services;

namespace Suoratoistopalvelut.Repositories
{
    public class Streaming_servicesContext : DbContext
    {
        public DbSet<User> Users { get; set; } = null!;
        public DbSet<Genre> Genres { get; set; } = null!;
        public DbSet<Repertory> Repertorys { get; set; } = null!;
        public DbSet<Service> Services { get; set; } = null!;

        public Streaming_servicesContext(DbContextOptions<Streaming_servicesContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasAlternateKey(r => r.UserName);
        }
    }
}
