﻿using Suoratoistopalvelut.Models;

namespace Suoratoistopalvelut.Repositories
{
    public interface IServiceRepository : IRepository<Service>
    {
        public Service? GetByServiceName(string serviceName);
    }
}
