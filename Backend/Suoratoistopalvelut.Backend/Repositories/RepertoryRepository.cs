﻿using Microsoft.EntityFrameworkCore;
using Suoratoistopalvelut.Models;

namespace Suoratoistopalvelut.Repositories
{
    public class RepertoryRepository : RepoBase<Repertory>, IRepertoryRepository
    {
        public RepertoryRepository(Streaming_servicesContext context) : base(context) { }

        public Repertory GetByRepertoryName(string name)
        {
            return _context.Set<Repertory>().Include("Genre").Include("Service").FirstOrDefault(entity => entity.Name == name);
        }

        public List<Repertory> GetAllRepertorys()
        {
            return _context.Set<Repertory>().Include("Genre").Include("Service").ToList();
        }

        public Repertory GetRepertoryById(int id)
        {
            return _context.Set<Repertory>().Include("Genre").Include("Service").FirstOrDefault(entity => entity.Id == id);
        }

        public List<Repertory> GetRepertorysByServiceId(int serviceId)
        {
            return _context.Set<Repertory>().Include("Genre").Include("Service").Where(entity => entity.Service.Id == serviceId).ToList();
        }

        public List<Repertory> GetRepertorysByGenreId(int genreId)
        {
            return _context.Set<Repertory>().Include("Genre").Include("Service").Where(entity => entity.Genre.Id == genreId).ToList();
        }

        public List<Repertory> GetRepertorysByGenreName(Genre? genreName)
        {
            return _context.Set<Repertory>().Include("Service").Where(entity => entity.Genre == genreName).ToList();
        }
        public List<Repertory> GetRepertorysByServiceName(Service? serviceName)
        {
            return _context.Set<Repertory>().Include("Genre").Where(entity => entity.Service == serviceName).ToList();
        }
    }
}
