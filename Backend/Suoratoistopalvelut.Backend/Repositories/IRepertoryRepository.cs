﻿using Suoratoistopalvelut.Models;

namespace Suoratoistopalvelut.Repositories
{
    public interface IRepertoryRepository : IRepository<Repertory>
    {
        public Repertory? GetByRepertoryName(string name);
        public List<Repertory> GetAllRepertorys();
        public Repertory? GetRepertoryById(int id);
        public List<Repertory> GetRepertorysByServiceId(int serviceId);
        public List<Repertory> GetRepertorysByGenreId(int genreId);
        public List<Repertory> GetRepertorysByGenreName(Genre? genreName);
        public List<Repertory> GetRepertorysByServiceName(Service? serviceName);
    }
}
