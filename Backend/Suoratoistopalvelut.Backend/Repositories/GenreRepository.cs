﻿using Suoratoistopalvelut.Models;

namespace Suoratoistopalvelut.Repositories
{
    public class GenreRepository : RepoBase<Genre>, IGenreRepository
    {
        public GenreRepository(Streaming_servicesContext context) : base(context) { }
        public Genre GetByGenreName(string genreName)
        {
            return _context.Set<Genre>().FirstOrDefault(entity => entity.GenreName == genreName);
        }
    }
}
