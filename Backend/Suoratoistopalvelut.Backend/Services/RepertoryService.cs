﻿using Suoratoistopalvelut.Models;
using Suoratoistopalvelut.Repositories;

namespace Suoratoistopalvelut.Services
{
    public class RepertoryService : IRepertoryService
    {
        private readonly IRepertoryRepository _repository;
        private readonly IGenreService _genreService;
        private readonly IServiceService _serviceService;

        public RepertoryService(IRepertoryRepository repository, IGenreService genreService, IServiceService serviceService)
        {
            _repository = repository;
            _genreService = genreService;
            _serviceService = serviceService;
        }

        public Repertory? GetByRepertoryName(string name)
        {
            return _repository.GetByRepertoryName(name);
        }

        public Repertory GetRepertoryById(int id)
        {
            return _repository.GetRepertoryById(id);
        }

        public List<Repertory> GetAllRepertorys()
        {
            return _repository.GetAllRepertorys();
        }

        public Repertory? Add(RepertoryData repertoryData)
        {
            if (_serviceService.GetByServiceName(repertoryData.ServiceName) == null)
            {
                _serviceService.Add(
                new Service
                {
                    ServiceName = repertoryData.ServiceName
                });
            }

            if (_genreService.GetByGenreName(repertoryData.GenreName) == null)
            {
                _genreService.Add(
                new Genre
                {
                    GenreName = repertoryData.GenreName
                });
            }

            var newRepertory = new Repertory
            {
                Name = repertoryData.Name,
                AddDay = repertoryData.AddDay,
                Serie_Movie = repertoryData.Serie_Movie,
                Service = _serviceService.GetByServiceName(repertoryData.ServiceName),
                Genre = _genreService.GetByGenreName(repertoryData.GenreName)
            };
            return _repository.Add(newRepertory);
        }

        public void Update(int id, RepertoryData repertoryData)
        {
            if (_serviceService.GetByServiceName(repertoryData.ServiceName) == null)
            {
                _serviceService.Add(
                new Service
                {
                    ServiceName = repertoryData.ServiceName
                });
            }

            if (_genreService.GetByGenreName(repertoryData.GenreName) == null)
            {
                _genreService.Add(
                new Genre
                {
                    GenreName = repertoryData.GenreName
                });
            }

            var modifiedRepertory = GetRepertoryById(id);
            modifiedRepertory.Name = repertoryData.Name;
            modifiedRepertory.AddDay = repertoryData.AddDay;
            modifiedRepertory.Serie_Movie = repertoryData.Serie_Movie;
            modifiedRepertory.Service = _serviceService.GetByServiceName(repertoryData.ServiceName);
            modifiedRepertory.Genre = _genreService.GetByGenreName(repertoryData.GenreName);
            _repository.Update(id, modifiedRepertory);
        }

        public Repertory? Delete(int id)
        {
            var deleteRepertory = GetRepertoryById(id);
            if (deleteRepertory != null)
            {
                _repository.Delete(id);
            }
            return deleteRepertory;
        }

        public List<Repertory> GetRepertorysByServiceId(int serviceId)
        {
            return _repository.GetRepertorysByServiceId(serviceId);
        }

        public List<Repertory> GetRepertorysByGenreId(int genreId)
        {
            return _repository.GetRepertorysByGenreId(genreId);
        }
        
        public List<Repertory> GetRepertorysByGenreName(Genre? genreName)
        {
            return _repository.GetRepertorysByGenreName(genreName);
        }
        public List<Repertory> GetRepertorysByServiceName(Service? serviceName)
        {
            return _repository.GetRepertorysByServiceName(serviceName);
        }
    }
}
