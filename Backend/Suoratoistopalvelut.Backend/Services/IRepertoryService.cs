﻿using Suoratoistopalvelut.Models;

namespace Suoratoistopalvelut.Services
{
    public interface IRepertoryService
    {
        public Repertory? GetByRepertoryName(string name);
        public List<Repertory> GetAllRepertorys();
        public Repertory GetRepertoryById(int id);
        public Repertory? Add(RepertoryData repertoryData);
        public void Update(int id, RepertoryData repertoryData);
        public Repertory? Delete(int id);
        public List<Repertory> GetRepertorysByServiceId(int serviceId);
        public List<Repertory> GetRepertorysByGenreId(int genreId);
        public List<Repertory>? GetRepertorysByGenreName(Genre? genreName);
        public List<Repertory>? GetRepertorysByServiceName(Service? serviceName);
    }
}
