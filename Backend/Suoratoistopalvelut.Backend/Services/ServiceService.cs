﻿using Suoratoistopalvelut.Models;
using Suoratoistopalvelut.Repositories;

namespace Suoratoistopalvelut.Services
{
    public class ServiceService : IServiceService
    {
        private readonly IServiceRepository _repository;

        public ServiceService(IServiceRepository repository)
        {
            _repository = repository;
        }

        public Service GetById(int id)
        {
            return _repository.GetById(id);
        }

        public List<Service> GetAll()
        {
            return _repository.GetAll();
        }

        public Service? Add(Service service)
        {
            var newService = new Service()
            {
                ServiceName = service.ServiceName
            };
            _repository.Add(newService);
            return service;
        }

        public void Update(int id, Service service)
        {
            var modifiedService = GetById(id);
            modifiedService.ServiceName = service.ServiceName;
            _repository.Update(id, modifiedService);
        }

        public Service? Delete(int id)
        {
            var deletedService = GetById(id);
            if (deletedService != null)
            {
                _repository.Delete(id);
            }
            return deletedService;
        }

        public Service? GetByServiceName(string serviceName)
        {
            return _repository.GetByServiceName(serviceName);
        }
    }
}
