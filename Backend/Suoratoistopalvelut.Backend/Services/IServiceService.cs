﻿using Suoratoistopalvelut.Models;

namespace Suoratoistopalvelut.Services
{
    public interface IServiceService
    {
        public Service GetById(int id);
        public List<Service> GetAll();
        public Service? Add(Service service);
        public void Update(int id, Service service);
        public Service? Delete(int id);
        public Service? GetByServiceName(string serviceName);
    }
}
