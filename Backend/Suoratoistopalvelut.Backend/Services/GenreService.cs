﻿using Suoratoistopalvelut.Models;
using Suoratoistopalvelut.Repositories;

namespace Suoratoistopalvelut.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _repository;

        public GenreService(IGenreRepository repository)
        {
            _repository = repository;
        }

        public Genre GetById(int id)
        {
            return _repository.GetById(id);
        }

        public List<Genre> GetAll()
        {
            return _repository.GetAll();
        }

        public Genre? Add(Genre genre)
        {
            var newGenre = new Genre()
            {
                GenreName = genre.GenreName
            };
            _repository.Add(newGenre);
            return genre;
        }

        public void Update(int id, Genre genre)
        {
            var modifiedGenre = GetById(id);
            modifiedGenre.GenreName = genre.GenreName;
            _repository.Update(id, modifiedGenre);
        }

        public Genre? Delete(int id)
        {
            var deletedGenre = GetById(id);
            if (deletedGenre != null)
            {
                _repository.Delete(id);
            }
            return deletedGenre;
        }

        public Genre? GetByGenreName(string genreName)
        {
            return _repository.GetByGenreName(genreName);
        }
    }
}
