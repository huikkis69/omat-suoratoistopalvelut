﻿using Suoratoistopalvelut.Models;

namespace Suoratoistopalvelut.Services
{
    public interface IGenreService
    {
        public Genre GetById(int id);
        public List<Genre> GetAll();
        public Genre? Add(Genre genre);
        public void Update(int id, Genre genre);
        public Genre? Delete(int id);
        public Genre? GetByGenreName(string genreName);
    }
}
